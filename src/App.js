import React, {Component} from 'react';
//bootstrap
import 'bootstrap/dist/css/bootstrap.css';
import './sass/App.sass';
import Container from "reactstrap/es/Container";
import Row from "reactstrap/es/Row";
import Col from "reactstrap/es/Col";
import {Button} from "reactstrap";
//components
import RegModal from "./components/RegModal";
import PeopleCard from "./components/PeopleCard";
import Navigation from "./components/Navigation";
//redux
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as modalAction from './actions/modalAction';
import * as usersAction from './actions/usersAction';
//library
import {Route, Switch} from "react-router-dom";
import PhotoModal from "./components/PhotoModal";
import axios from "axios";
import Settings from "./components/Settings";
import Login from "./components/Login";

const UserContent = (props) => {
    const {users, id} = props,
        {deleteUser, loadUsers} = props.usersAction,
        {togglePhoto} = props.modalAction,
        user = users.find(current => current.Id === id);

    const delete_photo = (uid) => {
        const id = window.location.href.split('/').pop(),
            link = `/api/person/${id}/pic/${uid}`;
        axios({
            url: link,
            method: 'DELETE',
            headers: {
                Authorization: `Basic YWRtaW46YWRtaW4=`
            }
        }).then(_ => {
            loadUsers();
        });
    };

    const trigger_status = (status) => {
        let bodyFormData = new FormData();
        bodyFormData.set('value', (!status).toString());

        const id = window.location.href.split('/').pop(),
            link = `/api/person/${id}/state`;
        axios({
            url: link,
            method: 'POST',
            headers: {
                Authorization: `Basic YWRtaW46YWRtaW4=`
            },
            data: bodyFormData
        }).then(_ => {
            loadUsers();
        });
    };
    if (user) {
        const {Caption, Status, Pic} = user;

        return (
            <Col xs={8} className={'user-content'}>
                <h2 className="name">{Caption}</h2>
                <div className={'buttons-content'}>
                    {Status ?
                        <Button outline color="warning" onClick={_ => {
                            trigger_status(Status)
                        }}>Disable</Button> :
                        <Button outline color="success" onClick={_ => {
                            trigger_status(Status)
                        }}>Enable</Button>
                    }
                    <Button outline color="danger" onClick={_ => {
                        deleteUser(id)
                    }}>Delete</Button>
                </div>
                <div className={'photos'}>
                    {Status ? Pic.map((item, i) => {
                        return <div className="photo" key={i}>
                            <div className="photo-hover">
                                <div className="add-photo">
                                    <Button outline color="primary" onClick={_ => {
                                        togglePhoto(i)
                                    }}>shot</Button>
                                    <Button outline color="primary" onClick={() => {
                                        delete_photo(i)
                                    }}>Delete</Button>
                                </div>
                            </div>
                            <img src={item} alt={'photo'}/>
                        </div>
                    }) : <span className={'message'}>user is disabled</span>}
                </div>
            </Col>
        );
    } else return (
        <Col xs={8} className={'user-content'}>
            <span className={'message'}>
                <i className="fas fa-arrow-left"> </i>
                Select or add a user
            </span>
        </Col>
    );
};

class RightPanel extends Component {


    componentDidMount() {
        this.props.usersAction.loadUsers();
    }

    render() {
        const {users} = this.props;
        const {toggleModal, togglePhoto} = this.props.modalAction;
        const {open, photo} = this.props.modal;
        return (
            <div id="App">
                <Navigation/>
                <Container>
                    <Row>
                        <Col xs={4} className={'people-list'}>
                            <div className={"p-card add-user"} onClick={toggleModal}>
                                <div className="p-content">
                                    <div className="image-wrapper">
                                        <i className="fas fa-user-plus"> </i>
                                    </div>
                                    <div className={'p-info'}>
                                        <p className="p-name">Add New User</p>
                                    </div>
                                </div>
                            </div>
                            {users.map(({Caption, Id, Pic, Status}, i) => {
                                return <PeopleCard name={Caption} userid={Id} key={i} photo={Pic[0]}
                                                   status={Status}/>
                            })}
                        </Col>
                        {this.props.children}
                    </Row>
                </Container>
                <RegModal open={open} toggle={toggleModal}
                          className={'reg-modal'}/>
                <PhotoModal open={photo} toggle={togglePhoto}
                            className={'reg-modal'}/>
            </div>
        );
    }
}

const UserPage = (props) => {
    return <RightPanel>
        <ConnectedUser id={props.match.params.id}/>
    </RightPanel>
};

const SettingsPage = () => {
    return <RightPanel>
        <Settings/>
    </RightPanel>
};

class App extends Component {
    render() {
        return (
            <Switch>
                <Route exact path='/' component={Login}/>
                <Route exact path='/panel/' component={RightPanel}/>
                <Route exact path='/settings' component={SettingsPage}/>
                <Route exact path='/panel/:id' component={UserPage}/>
            </Switch>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        modal: state.modal,
        users: state.users.users
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        modalAction: bindActionCreators(modalAction, dispatch),
        usersAction: bindActionCreators(usersAction, dispatch)
    }
};

RightPanel = connect(
    mapStateToProps,
    mapDispatchToProps
)(RightPanel);

const ConnectedUser = connect(
    mapStateToProps,
    mapDispatchToProps
)(UserContent);

export default App;
