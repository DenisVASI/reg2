import React from 'react';
import {
    Navbar,
    NavbarBrand
} from 'reactstrap';
import Container from "reactstrap/es/Container";
import Row from "reactstrap/es/Row";
import {NavLink} from 'react-router-dom'

function logout() {
    var xmlhttp;
    if (window.XMLHttpRequest) xmlhttp = new XMLHttpRequest();
    // else xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) window.location.href = "/";
    };
    xmlhttp.open("GET", "/api/person", true);
    xmlhttp.setRequestHeader("Authorization", "Basic YXNkc2E6");
    xmlhttp.send();
    return false;
}

export default class Navigation extends React.Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        return (
            <Navbar color="light" light expand="md">
                <Container>
                    <Row>
                        <NavbarBrand>Face Auth
                            <NavLink to="/settings"><i className="fas fa-cog"> </i></NavLink>
                            <a href="#"><i className="fas fa-sign-out-alt" onClick={() => {
                                logout();
                            }}> </i></a>
                        </NavbarBrand>
                    </Row>
                </Container>
            </Navbar>
        );
    }
}