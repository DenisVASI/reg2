import React from "react";

import {Field, reduxForm} from 'redux-form'
import DropdownList from 'react-widgets/lib/DropdownList';
import 'react-widgets/dist/css/react-widgets.css';
import * as settingsAction from "../actions/settingsActions";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import axios from 'axios';

const renderDropdownList = ({input, data, valueField, textField}) =>
    <DropdownList {...input}
                  data={data}
                  valueField={valueField}
                  textField={textField}
                  onChange={input.onChange}/>;

const levels = [
    {text: 'Blank', value: 0},
    {text: 'Error', value: 1},
    {text: 'Info', value: 3},
    {text: 'Detail', value: 4},
    {text: 'Full', value: 5}
];

const renderField = ({
                         input,
                         label,
                         type,
                         meta: {asyncValidating, touched, error}
                     }) => (
    <div className={'field-wrapper'}>
        <label>{label}</label>
        <div className={asyncValidating ? 'async-validating input-wrapper' : 'input-wrapper'}>
            <input {...input} type={type} placeholder={label}/>
            {touched && error && <span>{error}</span>}
        </div>
    </div>
);

class SettingsForm extends React.Component {

    componentDidMount() {
        this.props.settingsAction.loadSettings(
            _ => {
                this.props.initialize(this.props.settings);
            }
        );
    }

    submit = values => {
        let bodyFormData = new FormData();
        bodyFormData.set('FlagDebug', values.FlagDebug.toString());
        values.TypeLog.value ? bodyFormData.set('TypeLog', values.TypeLog.value.toString()) : bodyFormData.set('TypeLog', values.TypeLog.toString());
        bodyFormData.set('Login', values.Login.toString());
        bodyFormData.set('Password', values.Password.toString());
        bodyFormData.set('LockAddress', values.LockAddress.toString());
        axios({
            url: '/api/options',
            method: 'POST',
            data: bodyFormData,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then(_ => {
            this.props.settingsAction.loadSettings(
                _ => {
                    this.props.initialize(this.props.settings);
                }
            );
        });
    };

    render() {
        const {handleSubmit} = this.props;
        return (
            <form onSubmit={handleSubmit(this.submit.bind(this))}>


                <div className={'custom-checkbox'}>
                    <label htmlFor="firstName">Debug</label>
                    <Field name="FlagDebug" component="input" type="checkbox"/>
                </div>
                <div className={'custom-dropdown'}>
                    <label>Log type</label>
                    <Field
                        name="TypeLog"
                        component={renderDropdownList}
                        data={levels}
                        valueField="value"
                        textField="text"

                    />
                </div>

                <Field
                    name="Login"
                    component={renderField}
                    type="text"
                    label="login"
                />

                <Field
                    name="Password"
                    component={renderField}
                    type="text"
                    label="password"
                />

                <Field
                    name="LockAddress"
                    component={renderField}
                    type="text"
                    label="Lock Address"
                />

                <button type="submit" className={'btn btn-outline-primary'}>Save</button>
            </form>
        );
    }
}

SettingsForm = reduxForm({
    // a unique name for the form
    form: 'settings'
})(SettingsForm);

class Settings extends React.Component {
    render() {
        return (
            <div className="settings">
                <SettingsFormConn/>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    settingsAction: bindActionCreators(settingsAction, dispatch)
});

const mapStateToProps = (state) => {
    return {
        settings: state.settings
    }
};


const SettingsFormConn = connect(mapStateToProps, mapDispatchToProps)(SettingsForm);

export default Settings;