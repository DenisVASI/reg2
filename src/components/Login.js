import React from "react";
import {NavLink} from "react-router-dom";
import axios from "axios";

class Login extends React.Component{
    constructor(){
        super();
        this.state = {
            border: 'auth-border',
        }
    }
    componentDidMount() {
        // this.interval = setInterval(()=>{
        //     axios({
        //         url: '/api/status',
        //         headers: {
        //             'content-type': 'application/json'
        //         },
        //         responseType: 'json'
        //     }).then((res)=>{
        //         if(parseInt(res.data.Status) === 1){
        //             this.setState({border: 'auth-border success'})
        //         }
        //         else{
        //             this.setState({border: 'auth-border'})
        //         }
        //         this.setState({name: res.data.Value})
        //     }).catch((err) => {
        //         console.log(err);
        //     });
        // },750);
        this.ws = new WebSocket(`ws://${window.location.host}/ws`);
        this.ws.onmessage = (event) => {
            let data = JSON.parse(event.data);
            if(data.Status) data.border = 'auth-border success';
            else data.border = 'auth-border';
            this.setState(data);
        };
    }

    render() {
        return(
            <div className="login">
                <div className="wrapper">
                    <div className="logo">face auth <NavLink to="/panel"><i className="fas fa-user-cog" onClick={()=>{
                        this.ws.close();
                    }}> </i></NavLink></div>
                    <div className="view">
                        <div className="cam">
                            <div className={this.state.border}>
                                <div className="status">{this.state.Value}</div>
                            </div>
                            <img src="/api/cam" alt="cam stream"/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;